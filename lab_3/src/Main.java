import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Node node;
        System.out.println(Arrays.toString(args));
        if ( args.length == 3 ) {
            node = new Node(args[0], Integer.valueOf(args[1]),Integer.valueOf(args[2]));
        }
        else if ( args.length == 5 ) {
            node = new Node(args[0], Integer.valueOf(args[1]),Integer.valueOf(args[2]),
                    Integer.valueOf(args[3]), args[4]);
        }
        else{
            System.err.println("Usage: REQUIRED [nodeName] [missPercent] [nodePort]" +
                    " OPTIONAL [parentPort] [parentIP]");
            return;
        }
        node.run();
    }
}
