import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Node  {
    Node(String name, int lostOfPercent, int myPort) {
        this.name = name;
        this.lostOfPercent = lostOfPercent;
        try {
            datagramSocket = new DatagramSocket(myPort);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        timerForMissingMessage.schedule(checkMissingMessage, 1000, 3000);
        timerForPinging.schedule(pinging, 0, 1000);
        senderThread.start();
    }

    Node(String name, int lostOfPercent, int myPort, int parentPort, String parentIp ) {
        this(name, lostOfPercent, myPort);
        this.parentPort = parentPort;
        try {
            this.parentIp = InetAddress.getByName(parentIp);
            connect(REQUEST_ADOPT, parentPort, this.parentIp);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public  void  run(){
        try {
            while (true){
                DatagramPacket requestPacket = new DatagramPacket(new byte[BUFFER_SIZE], BUFFER_SIZE);
                datagramSocket.receive(requestPacket);
                int portSender = requestPacket.getPort();
                InetAddress ipSender = requestPacket.getAddress();

                switch (requestPacket.getData()[0]){
                    case REQUEST_ADOPT:
                        System.out.println("Усыновите меня : " + portSender + "  " + ipSender);
                        adoption(portSender, ipSender);
                        break;
                    case SUCCESSFUL_ADOPTION:
                        System.out.println("Меня усыновили:) - Шлю смс");
                        break;
                    case REQUEST_SEND:
                        if ( !iKnowHim(portSender)){
                            continue;
                        }
                        byte[] receiverMsg = requestPacket.getData();
                        int lengthMsg = getLengthMessage(receiverMsg);
                        byte[] msg = getMessage(receiverMsg, lengthMsg);
                        UUID uuidMsg = getUuid(receiverMsg, lengthMsg);
                        if ( messageIsLost() ) {
                            System.out.println("Сообщение потеряно " + new String(msg) );
                        }
                        else {
                            lastConnection.put(portSender,System.currentTimeMillis());

                            System.out.println(new String(msg));

                            sendConfirmation(uuidMsg, portSender, ipSender);

                            addToDeliveredMessages(receiverMsg, uuidMsg, portSender);

                            sendMessageRelatives(receiverMsg, portSender);

                        }
                        break;
                    case MESSAGE_DELIVERED:
                        lastConnection.put(portSender,System.currentTimeMillis());
                        markDelivered(requestPacket.getData(), portSender, ipSender);
                        break;
                    case PING:
                        lastConnection.put(portSender,System.currentTimeMillis());
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean iKnowHim(int portSender) {
        if ( portSender == parentPort ) {
            return true;
        }
        for (Map.Entry<Integer, InetAddress> entry: children.entrySet()){
            if ( entry.getKey() == portSender ){
                return true;
            }
        }
        return  false;
    }

    private void sendMessageRelatives(byte[] msg, int portSender) {
        sendParent(portSender, msg);
        sendChildren(portSender, msg);
    }

    private void addToDeliveredMessages(byte[] msg, UUID uuidMsg, int portSender) {
        deliveredMessage.put(uuidMsg, msg);
        List<Map.Entry<Integer,InetAddress>> currentRelatives = getCurrentRelatives(portSender);
        if ( !currentRelatives.isEmpty() ){
            missingMessage.put(uuidMsg,currentRelatives);
        }
    }

    private void sendConfirmation(UUID uuidMsg, int portSender, InetAddress ipSender) {
        ByteBuffer buffer = ByteBuffer.allocate(1 + UUID_SIZE);
        buffer.put(MESSAGE_DELIVERED);
        buffer.put(asBytes(uuidMsg));
        DatagramPacket confirmationPacket = new DatagramPacket(buffer.array(), buffer.array().length, ipSender, portSender);
        try {
            datagramSocket.send(confirmationPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] asBytes(UUID uuid) {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }

    private UUID getUuid(byte[] receiverMsg, int lengthMsg) {
        byte[] uuidBuf = new byte[UUID_SIZE];
        System.arraycopy(receiverMsg,  5 + lengthMsg, uuidBuf,0,UUID_SIZE);
        return asUuid(uuidBuf);
    }

    private byte[] getMessage(byte[] receiverMsg, int lengthMsg) {
        byte[] msg = new byte[lengthMsg];
        System.arraycopy(receiverMsg,  5, msg,0,lengthMsg);
        return msg;
    }

    private int getLengthMessage(byte[] receiverMsg) {
        ByteBuffer buffer = ByteBuffer.wrap(receiverMsg,1, 4);
        return  buffer.getInt();
    }

    private void adoption(int portSender, InetAddress ipSender) {
        children.put(portSender,ipSender);
        connect(SUCCESSFUL_ADOPTION,portSender,ipSender);
    }

    private UUID asUuid( byte [] uuid) {
        ByteBuffer uuidBuf = ByteBuffer.wrap(uuid);
        long firstLong = uuidBuf.getLong();
        long secondLong = uuidBuf.getLong();
        return new UUID(firstLong, secondLong);
    }

    private void markDelivered(byte[] msgBuf, int port, InetAddress ip) {
        byte[] uuidMsgBuf = new byte[UUID_SIZE];
        System.arraycopy(msgBuf, 1, uuidMsgBuf, 0, UUID_SIZE);
        UUID uuidMsg = asUuid(uuidMsgBuf);

        List<Map.Entry<Integer,InetAddress>> i = missingMessage.get(uuidMsg);
        i.remove(new AbstractMap.SimpleEntry(port, ip));
        if ( i.isEmpty() ){
            missingMessage.remove(uuidMsg);
            deliveredMessage.remove(uuidMsg);
        }
        else {
            missingMessage.put(uuidMsg, i);
        }
    }

    private boolean messageIsLost(){
        int lostPacket = ThreadLocalRandom.current().nextInt(0, 100);
        return lostPacket < lostOfPercent;
    }

    private void sendChildren(int portSender, byte[] msg) {
        for (Map.Entry<Integer, InetAddress> entry : children.entrySet()) {
            if ( portSender == FIRST_SEND || portSender != entry.getKey()) {
                send(msg,entry.getKey(), entry.getValue());
            }
        }
    }

    private void send(byte[] msg,int portReceiver, InetAddress ipReceiver) {
        try {
            DatagramPacket msgPacket = new DatagramPacket(msg, msg.length, ipReceiver, portReceiver);
            datagramSocket.send(msgPacket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendParent(int portSender, byte[] msg) {
        if ( parentPort != 0 && (portSender == FIRST_SEND || portSender != parentPort)  ) {
            send(msg,parentPort, parentIp);
        }
    }

    private void connect(byte request, int portReceiver, InetAddress ipReceiver) {
        try {
            byte [] buffer = {request};
            DatagramPacket requestPacket = new DatagramPacket(buffer,buffer.length, ipReceiver, portReceiver);
            datagramSocket.send(requestPacket);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteInactiveKeys(ArrayList<Integer> keys) {
        for( Integer i: keys ){
            lastConnection.remove(i);
        }
    }

    private byte[] createMessages(String read) {
        String msg = name + ": "  + read;
        UUID uuidMsg = UUID.randomUUID();
        // id (type) + int + msg + uuid
        ByteBuffer msgBuffer = ByteBuffer.allocate(5 + msg.getBytes().length + 16);
        msgBuffer.put(REQUEST_SEND);
        msgBuffer.putInt(msg.getBytes().length);
        msgBuffer.put(msg.getBytes());
        msgBuffer.putLong(uuidMsg.getMostSignificantBits());
        msgBuffer.putLong(uuidMsg.getLeastSignificantBits());
        addToDeliveredMessages(msgBuffer.array(),uuidMsg, FIRST_SEND);
        return msgBuffer.array();
    }

    private List<Map.Entry<Integer,InetAddress>> getCurrentRelatives( int ignorePort) {
        ArrayList<Map.Entry<Integer,InetAddress>> tmp = new ArrayList<>();
        if ( parentPort != 0  && parentPort != ignorePort) {
            tmp.add(new AbstractMap.SimpleEntry(parentPort, parentIp));
        }
        for (Map.Entry<Integer, InetAddress> entry : children.entrySet()) {
            if ( ignorePort != entry.getKey()){
                tmp.add(entry);
            }
        }
        return tmp;
    }

    private DatagramSocket datagramSocket = null;

    private int lostOfPercent;
    private String name;
    private int parentPort = 0;
    private InetAddress parentIp;
    private HashMap<Integer, InetAddress> children = new HashMap<>();

    private final byte REQUEST_ADOPT = 0;
    private final byte SUCCESSFUL_ADOPTION = 1;
    private final byte REQUEST_SEND = 2;
    private final byte MESSAGE_DELIVERED = 3;
    private final byte PING = 4;
    private final int FIRST_SEND = 0;

    private final int BUFFER_SIZE = 4096;
    private final int UUID_SIZE = 16;
    private Timer timerForMissingMessage = new Timer();
    private Timer timerForPinging = new Timer();

    private HashMap<Integer, Long> lastConnection = new HashMap<>();
    private HashMap<UUID,List<Map.Entry<Integer, InetAddress>>> missingMessage = new HashMap<>();
    private HashMap<UUID, byte[]> deliveredMessage = new HashMap<>();

    private Thread senderThread = new Thread(() -> {
        while (true){
            System.out.println("->");
            Scanner in = new Scanner(System.in);
            String read = in.nextLine();
            byte[] msg = createMessages(read);
            sendMessageRelatives(msg, FIRST_SEND);
        }

    });


    private TimerTask pinging = new TimerTask() {
        @Override
        public void run() {
            byte[] msg ={PING};
            sendParent(FIRST_SEND, msg);
            sendChildren(FIRST_SEND, msg);
            Long currentTime = System.currentTimeMillis();
            ArrayList<Integer> inactiveConnection = new ArrayList<>();
            for (Map.Entry<Integer, Long> entry : lastConnection.entrySet()) {
                if ( currentTime - entry.getValue() > 5000) {
                    if (parentPort == entry.getKey() ) {
                        System.out.println(" Родитель вас покинул :" + parentPort );
                        parentPort = 0;
                        parentIp = null;
                    }
                    else{
                        System.out.println("Потерялся ребенок: "+  entry.getKey());
                        children.remove(entry.getKey());
                    }
                    inactiveConnection.add(entry.getKey());
                }
            }
            deleteInactiveKeys(inactiveConnection);
            deleteInactiveKeys(inactiveConnection);
        }
    };


    private TimerTask checkMissingMessage = new TimerTask() {
        @Override
        public void run() {
            for (Map.Entry<UUID, List<Map.Entry<Integer,InetAddress>> > entry: missingMessage.entrySet()) {
                for( Map.Entry<Integer, InetAddress> i : entry.getValue()) {
                    int port = i.getKey();
                    if( lastConnection.containsKey(port)){
                        send(deliveredMessage.get(entry.getKey()), port, i.getValue());
                    }
                }
            }
        }
    };


}
